FROM mcr.microsoft.com/powershell:latest

RUN pwsh -Command "Set-PSRepository -Name PSGallery -InstallationPolicy Trusted" \
    && pwsh -Command "Install-Module -Name VMware.PowerCLI -Force -Scope AllUsers"

WORKDIR /app
COPY snap.ps1 .

CMD ["powershell", "./snap.ps1"]