# Read-Host :Permet une interaction entre le terminal et l'utilisateur 
# VM_Name : Variable qui stocke le nom de la VM 
# Snap_Name : Variable qui stocke le nom du Snapshot


# credential : Variable qui importe les identifiant d'authentification du server esxi 
$credential = Import-Clixml -Path "MyCredential2.xml"

# Connect-VIServer : Tente la connexion aus serveur ESXI 
Connect-VIServer -Server 10.5.29.20 -Credential $credential

# Set-VM : Restauration de snapshot en Fonction de Nom de VM et du Snapshot 
Set-VM -VM windows -Snapshot (Get-Snapshot -VM windows -Name 'TEST') -Confirm:$false